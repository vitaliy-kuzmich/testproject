package controller

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"runtime"
	"testProject/src/service"
	"testProject/src/validator"
)

type FController struct {
	*service.FService
}

func (this *FController) Calculate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	log.Println("validating input data...")
	d, err := validator.ValidateFInput(r.Body)
	if d != nil {
		log.Println("decoded A", d.A)
		marsh, err := json.Marshal(this.Calc(d, runtime.NumCPU()))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write(marsh)
		}
	} else {
		log.Println("error during calculation")
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

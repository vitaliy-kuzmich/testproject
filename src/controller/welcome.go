package controller

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func Welcome(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

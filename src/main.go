package main

import (
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"testProject/src/controller"
	"testProject/src/service"
)

var fContrloller *controller.FController

func init() {
	fContrloller = &controller.FController{&service.FService{}}
}
func main() {
	router := httprouter.New()
	router.GET("/", controller.Welcome)
	router.POST("/calculate", fContrloller.Calculate)
	log.Print("Listening 8989")
	log.Fatal(http.ListenAndServe(":8989", router))
}

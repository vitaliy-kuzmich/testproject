package mapper

import "testProject/src/dto"

func Convert(a, b int64) *dto.SomeBigNumber {
	return &dto.SomeBigNumber{A: a, B: b}
}

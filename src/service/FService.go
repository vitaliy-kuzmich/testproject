package service

import (
	"log"
	"sync"
	"testProject/src/dto"
	"testProject/src/mapper"
	"testProject/src/utils"
)

// it is possible to do it without struct, but will leave for potential extensions
type FService struct {
}

func (this *FService) Calc(dto *dto.FDto, coresCount int) *dto.SomeBigNumber {
	log.Println("calculating for cores count ", coresCount)
	if coresCount > dto.A || coresCount > dto.B {
		coresCount = 2
		log.Println("cores count more then factorial digit, fallback to 2 cores ")
	}
	return mapper.Convert(this.calcInner_(dto.A, coresCount), this.calcInner_(dto.B, coresCount))
}
func (this *FService) calcInner_(num int, cc int) (f int64) {
	gapLimit := utils.CalcGapLimit(num, cc)
	log.Println("gapLimit ", gapLimit)
	log.Println("num ", num)
	calcedInGo := make(chan int64, cc)
	defer func() { f = Summarize(calcedInGo) }()
	var wg sync.WaitGroup
	wg.Add(cc)
	for i := 0; i < cc; i++ {
		go func(i int) { calcedInGo <- this.calcInner(gapLimit, i, num); wg.Done() }(i)
	}
	wg.Wait()
	close(calcedInGo)
	log.Println("final ", f)
	return
}
func Summarize(ch chan int64) (f int64) {
	f = 1
	for v := range ch {
		f *= v
	}
	return
}
func (this *FService) calcInner(gapLimit int, i int, num int) (res int64) {
	res = 1
	sm := gapLimit * i
	if sm == 0 {
		sm = 1
	} else {
		sm += 1
	}
	currentStartNum := int64(sm)
	log.Println("currentStartNum ", currentStartNum)
	var tmpRes int64
	for j := 0; j < gapLimit; j++ {
		tmpRes = currentStartNum + int64(j)
		res *= tmpRes
		log.Println("inner calc i ", j, tmpRes)
		if tmpRes == int64(num) {
			break
		}
	}
	return res
}

// :todo test me
var CalcInner = (*FService).calcInner

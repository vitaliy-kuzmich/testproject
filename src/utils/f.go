package utils

import "log"

func CalcGapLimit(num int, cc int) (gapLimit int) {
	log.Printf("calculating gap limit for num %d, and cc %d", num, cc)
	var rs float32
	rs = float32(num) / float32(cc)
	log.Println("rs", rs)
	var whole int = int(rs)
	log.Println("whole", whole)
	if rs-float32(whole) > 0 {
		gapLimit = whole + 1
	} else {
		gapLimit = whole
	}
	if gapLimit == 0 {
		gapLimit = 1
	}
	return
}

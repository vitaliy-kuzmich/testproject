package validator

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"testProject/src/dto"
)

const ErrMsg = "error:Incorrect input"

func ValidateFInput(body io.ReadCloser) (res *dto.FDto, err error) {
	err_ := json.NewDecoder(body).Decode(&res)
	if err_ != nil || res.A <= 0 || res.B <= 0 {
		err = fmt.Errorf(ErrMsg + " or negative a or b")
		res = nil
		if err_ != nil {
			log.Println(err.Error())
		}
	}
	return
}
